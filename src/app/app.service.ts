import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AppService {
  apiUrl=localStorage.getItem("url")
  constructor(private http: HttpClient) { 
    this.apiUrl = localStorage.getItem('url')
    console.clear()
  }
  

  login(form: any){
    var api = `${this.apiUrl}/users/auth`
    var header = new HttpHeaders
    header.append("Content-Type", "application/json");
    return this.http.post(api, form, {headers:header})
  }
}

