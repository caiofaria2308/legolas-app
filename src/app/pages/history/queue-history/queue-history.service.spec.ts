import { TestBed } from '@angular/core/testing';

import { QueueHistoryService } from './queue-history.service';

describe('QueueHistoryService', () => {
  let service: QueueHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QueueHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
