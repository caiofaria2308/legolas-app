import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryRoutingModule } from './history-routing.module';

import {QueueHistoryComponent} from './queue-history/queue-history.component'
import {VacationComponent} from './vacation/vacation.component'

import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTableModule } from 'ng-zorro-antd/table';




@NgModule({
  declarations: [
    QueueHistoryComponent,
    VacationComponent
  ],
  imports: [
    CommonModule,
    NzDrawerModule,
    NzButtonModule,
    NzSpinModule,
    NzTableModule,
    HistoryRoutingModule
  ],exports:[
    QueueHistoryComponent,
    VacationComponent
  ]
})
export class HistoryModule { }
