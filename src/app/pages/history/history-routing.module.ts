import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {QueueHistoryComponent} from './queue-history/queue-history.component'
import {VacationComponent} from './vacation/vacation.component'

const routes:Routes = [
  {
    path:'queue-history',
    component: QueueHistoryComponent
  },{
    path:'vacation',
    component:VacationComponent
  }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
