import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QueueService {

  apiUrl=localStorage.getItem("url")
  constructor(private http: HttpClient) { 
    this.apiUrl = localStorage.getItem('url')
    console.clear()
  }
}
