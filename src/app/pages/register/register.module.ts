import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterRoutingModule } from './register-routing.module';

import {ClientComponent} from './client/client.component'
import {ProductComponent} from './product/product.component'
import {QueueComponent} from './queue/queue.component'
import { UserComponent } from './user/user.component';

import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTableModule } from 'ng-zorro-antd/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';


@NgModule({
  declarations: [
    ClientComponent,
    ProductComponent,
    QueueComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    NzDrawerModule,
    NzButtonModule,
    NzSpinModule,
    NzTableModule,
    FormsModule,
    ReactiveFormsModule,
    NzFormModule,
    NzDropDownModule
  ],
  exports: [
    ClientComponent,
    ProductComponent,
    QueueComponent,
    UserComponent
  ],
})
export class RegisterModule { }
