import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserComponent } from './user/user.component';
import { ClientComponent } from './client/client.component';
import { ProductComponent } from './product/product.component';
import { QueueComponent } from './queue/queue.component';

const routes: Routes = [
  {
    path:'user',
    component: UserComponent
  },{
    path:'client',
    component: ClientComponent
  },{
    path:'product',
    component: ProductComponent
  },{
    path:'queue',
    component: QueueComponent
  }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
