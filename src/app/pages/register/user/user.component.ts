import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {

  isLoadingTable = false
  listUsers: User[] = []

  openClient = false
  loadingOpenClient = false

  editClient = false
  loadingEditClient = false
  constructor(private app: UserService) { }

  ngOnInit(): void {
    this.isLoadingTable = true
    this.app.get_all().toPromise().then(
      (response: any)=>{
        var users = response.users
        users.forEach((user :any) => {
          var created = new Date(user.created_date)
          var updated = new Date(user.updated_date)
          var tmp = {
            "_id": user._id,
            "name": user.name,
            "is_admin":user.is_admin ? "Sim" : "Não",
            "created_date": `${created.getDate()}/${created.getMonth()}/${created.getFullYear()} ${created.getHours()}:${created.getMinutes()}`,
            "updated_date": `${updated.getDate()}/${updated.getMonth()}/${updated.getFullYear()} ${updated.getHours()}:${updated.getMinutes()}`
          } 
          this.listUsers.push(tmp)
        });
        this.isLoadingTable = false
      }
    )
  }
  open_client(id: string): void{
    this.openClient = true
    this.loadingOpenClient = true
    return
  } 
  edit_client(id: string): void{
    this.editClient = true
    this.loadingEditClient = true
    return
  }

}
interface User {
  _id: string
  name: string
  is_admin: string
  created_date: string
  updated_date: string

}
