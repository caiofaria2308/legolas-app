import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl=localStorage.getItem("url")
  token = localStorage.getItem("token")
  constructor(private http: HttpClient) { 
    this.apiUrl = localStorage.getItem('url')
    this.token = sessionStorage.getItem("token")
    console.clear()
  }

  get_all(){

    var api = `${this.apiUrl}/users`
    var header = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${this.token}`
    })
    return this.http.get(api,{headers:header})
  }
}
