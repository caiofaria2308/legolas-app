import { Component } from '@angular/core';
import {CommonService} from './common.service';
import {AppService} from './app.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  isCollapsed = false;
  isLogged = false;
  isLoadingLogin = false;

  name_user: string = ""
  
  loading_login: boolean = false
  message_login= ""
  validateForm!: FormGroup;
  constructor(private common: CommonService, private fb: FormBuilder, private app: AppService){
      var logado = common.verifyLogin()
      if(logado){
        var token  = sessionStorage.getItem('token')
        if(token != null){
          common.getme(token).toPromise().then(
            (data:any)=>{
              if(data.status){
                this.isLogged = true
                this.name_user = data.user.name
              }else{
                this.isLogged = false
              }
            }
          )
        }
        
      }
  }

  ngOnInit(): void{
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })
  }

  sair():void {
    sessionStorage.removeItem("token")
    this.isLogged = false
    this.name_user = ""
  }
  logar():void{
    this.isLoadingLogin = true
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
    var response;
    this.app.login(this.validateForm.value).toPromise().then(
      (data: any) => {
        if(data.status == true){
          sessionStorage.setItem('token', data.token)
          location.href = location.href
          this.message_login = ""
          this.isLoadingLogin = false
        }else{
          this.message_login = "Usuário ou senha incorreta"
          this.isLoadingLogin = false
        }
      }
    )
  
  }


}


