import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CommonService {
  apiUrl = "http://localhost:8000/api";
  constructor(
    private http: HttpClient
  ) {    
    localStorage.setItem('url', this.apiUrl)
  }
  public verifyLogin(): any{
    var token = sessionStorage.getItem('token');
    if(token){
      sessionStorage.setItem('last_url', location.href)
      return true
    }else{
      sessionStorage.setItem('last_url', location.href);
      sessionStorage.removeItem('token')
      return false
    }
  } 
  public getme(token: string){
      var api = `${this.apiUrl}/users/auth/`
      var header = new HttpHeaders({
        'Content-Type': "application/json",
        Authorization: `Bearer ${token}`
      })

      return this.http.get(api, {headers:header});
  }
}
